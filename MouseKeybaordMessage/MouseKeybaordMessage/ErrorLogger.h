#pragma once
#include <windows.h>
#include <string>
#include <comdef.h>


class ErrorLogger
{
public:

#ifdef UNICODE
	static void ErrorLogger::Log(HRESULT hr, std::string message)
	{
		_com_error error(hr);
		wchar_t* wmsg = ASCI_2_WCHAR(message.c_str());
		std::wstring error_message = L"Error: " + std::wstring(wmsg) + L"\n" + error.ErrorMessage();
		free(wmsg);
		MessageBoxW(NULL, error_message.c_str(), L"Error", MB_ICONERROR);
	}
	static void ErrorLogger::Log(std::string message)
	{
		wchar_t* wmsg = ASCI_2_WCHAR(message.c_str());
		std::wstring error_message = L"Error: " + std::wstring(wmsg) + L"\n";
		free(wmsg);
		MessageBoxW(NULL, error_message.c_str(), L"Error", MB_ICONERROR);
	}
#else
	static void ErrorLogger::Log(HRESULT hr, std::string message)
	{
		_com_error error(hr);
		std::string error_message = std::string("Error: ") + message + "\n" + error.ErrorMessage();
		MessageBoxA(NULL, error_message.c_str(), "Error", MB_ICONERROR);
	}

	static void ErrorLogger::Log(std::string message)
	{
		std::string error_message = std::string("Error: ") + message + "\n";
		MessageBoxA(NULL, error_message.c_str(), "Error", MB_ICONERROR);
	}
#endif // UNICODE

	static wchar_t* ASCI_2_WCHAR(const char* asciCode)
	{
		int len = MultiByteToWideChar(CP_ACP, 0, asciCode, -1, NULL, NULL);
		if (len <= 0 || !asciCode)
			throw;
		wchar_t* dst = (wchar_t*)malloc(len * sizeof(wchar_t));
		len = MultiByteToWideChar(CP_ACP, 0, asciCode, -1, dst, len);
		return dst;
	}
};