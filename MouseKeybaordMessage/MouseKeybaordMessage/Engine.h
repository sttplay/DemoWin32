#pragma once
#include "ErrorLogger.h"
class Engine
{
public:
	Engine(HINSTANCE hInstance, std::string window_class);
	~Engine();

	bool Initialize(std::string window_title, int width, int height, int nCmdShow);

	bool CreateNewWindowShow(int nCmdShow = SW_SHOWDEFAULT);

	bool CreateNewWindow();

	bool ProcessMessages();

	HWND GetHWND();

	void Close();
private:

	bool RegisterWindowClass();

	static LRESULT CALLBACK HandleMessageSetup(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	static LRESULT CALLBACK HandleMsgRedirect(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	LRESULT CALLBACK WindProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);


	void OnStart();

	void OnClose();
private:
	HWND handle = NULL;
	HINSTANCE hInstance = NULL;
	std::string window_title = "";
	std::wstring window_title_wide = L"";
	std::string window_class = "";
	std::wstring window_class_wide = L"";
	int width = 0;
	int height = 0;
};