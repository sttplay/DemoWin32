#include "Engine.h"

//windows or console
#pragma comment( linker, "/subsystem:\"console\" /entry:\"wWinMainCRTStartup\"" )

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{

	Engine engine(hInstance, "WindowClass");
	if (!engine.Initialize("Window", 800, 600, nCmdShow))
		ErrorLogger::Log("��ʼ��ʧ��");

	while (engine.ProcessMessages())
	{
		Sleep(10);
	}

	return 0;
}